def printUsage
	scriptName = File.basename $0
	STDERR.puts "Usage: ruby #{scriptName} TARGET"
end

if ARGV.size < 1 or ARGV.size > 1
	printUsage
	exit
end
targetDirName = File.expand_path ARGV[0]

CASES_FIXED = [ \
	[    \
		[100,   0, 0]  \
	], [ \
		[100,   0, 2], \
		[100,   0, 1], \
		[100,   0, 0]  \
	], [ \
		[100,   0, 2], \
		[100,  10, 1], \
		[100,  10, 0]  \
	], [ \
		[300,   0, 2], \
		[200,  10, 1], \
		[100,  10, 0]  \
	], [ \
		[100,   0, 0], \
		[100, 200, 0] \
	], [ \
		[100, 200, 0] \
	], [ \
		[100,   0, 9], \
		[100,  10, 8], \
		[100,  10, 7], \
		[100,  10, 6], \
		[100,  10, 5], \
		[100,  10, 4], \
		[100,  10, 3], \
		[100,  10, 2], \
		[100,  10, 1], \
		[100,  10, 0] \
	], [ \
		[200,   0, 9], \
		[190,  10, 8], \
		[180,   0, 7], \
		[170,  10, 6], \
		[160,   0, 5], \
		[150,  10, 4], \
		[140,   0, 3], \
		[130,  10, 2], \
		[120,   0, 1], \
		[100,  10, 0] \
	], [ \
		[100,   0, 0], \
		[100, 105, 0] \
	], [ \
		[300,   0, 0], \
		[200,  90, 1], \
		[100,   0, 2]  \
	] ]

NUM_CASES_S = 30
NUM_CASES_L = 10
SIZE_S = 30
SIZE_L = 300

# Make directory
begin
	if not File.directory? targetDirName
		Dir.mkdir targetDirName
		puts "Make directory: " + targetDirName
	end
rescue
	STDERR.puts "Cannot create target directory: " + targetDirName
	exit
end

# Check directory, should be empty
targetDirContents = nil
begin
	targetDirContents = Dir.entries targetDirName
	filePtr = File.open(File.expand_path(targetDirName + "/file"), "w")
	filePtr.close
rescue
	STDERR.puts "Cannot access target directory: " + targetDirName
	exit
end
targetDirContents.delete_if {|filename| filename == "." or filename == ".."}
if targetDirContents.size > 0
	STDERR.puts "Found a dirty target directory: " + targetDirName
	exit
end
puts "Will write to target directory: " + targetDirName

# Generate test cases
puts "Prepared #{CASES_FIXED.size} fixed case(s)"

casesSmall = []
for j in 0...NUM_CASES_S
	caseItem = []
	for k in 0...SIZE_S
		process = [rand(500) + 1, rand(200), rand(10)]
		caseItem << process
	end
	casesSmall << caseItem
end
puts "Prepared #{casesSmall.size} small case(s)"

casesLarge = []
for j in 0...NUM_CASES_L
	caseItem = []
	for k in 0...SIZE_L
		process = [rand(500) + 1, rand(200), rand(10)]
		caseItem << process
	end
	casesLarge << caseItem
end
puts "Prepared #{casesLarge.size} large case(s)"

# Write cases
cases = CASES_FIXED + casesSmall + casesLarge
for j in 0...cases.size
	fileName = "case_#{j + 1}.jobs.txt"
	caseItem = cases[j]
	filePtr = File.open(File.expand_path(targetDirName + "/" + fileName), "w")
	for k in 0...caseItem.size
		process = caseItem[k]
		filePtr.puts process.map(&:to_s).join(" ")
	end
	filePtr.close
end
puts "Wrote #{cases.size} case(s)"

