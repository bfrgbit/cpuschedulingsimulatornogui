/** RoundRobinSchedulingAlgorithm.java
 * 
 * A scheduling algorithm that randomly picks the next job to go.
 *
 * @author: Kyle Benson
 * Winter 2013
 *
 */
package com.jimweller.cpuscheduler;

import java.util.*;

public class RoundRobinSchedulingAlgorithm extends BaseSchedulingAlgorithm {

    private Vector<Process> jobs;

    /** the time slice each process gets */
    private int quantum;

    /**
     * A count down to when to interrupt a process because it's time slice is
     * over.
     */
    private int quantumCounter;

    /** The index into the vector/array/readyQueue. */
    private int activeIndex;

    RoundRobinSchedulingAlgorithm() {
		activeJob = null;
		quantum = 10;
		quantumCounter = quantum;
		activeIndex = -1;
		jobs = new Vector<Process>();
    }

    /** Add the new job to the correct queue. */
    public void addJob(Process p) {
    	jobs.add(p);
    }

    /** Returns true if the job was present and was removed. */
    public boolean removeJob(Process p) {
		int jobIndex = jobs.indexOf(p);
		boolean ret = jobs.remove(p);
		
		if(activeIndex >= jobIndex && jobIndex >= 0){
			if(activeIndex == jobIndex) {
				quantumCounter = 0; // So that we know to preempt next cycle
			}
		    activeIndex--;
		    try {
		    	activeJob = jobs.get(activeIndex);
		    } catch (ArrayIndexOutOfBoundsException e){
		    	activeJob = null;
		    }
		}
		return ret;
    }

    /** Transfer all the jobs in the queue of a SchedulingAlgorithm to another, such as
	when switching to another algorithm in the GUI */
    public void transferJobsTo(SchedulingAlgorithm otherAlg) {
    	throw new UnsupportedOperationException();
    }

    /**
     * Get the value of quantum.
     * 
     * @return Value of quantum.
     */
    public int getQuantum() {
    	return quantum;
    }

    /**
     * Set the value of quantum.
     * 
     * @param v
     *            Value to assign to quantum.
     */
    public void setQuantum(int v) {
    	this.quantum = v;
    }

    /**
     * Returns the next process that should be run by the CPU, null if none
     * available.
     */
    public Process getNextJob(long currentTime) {
		quantumCounter--;
	
		// Not time to preempt
		if(activeIndex >= 0 && !isJobFinished() && quantumCounter > 0) {
		    return activeJob;
		}
	
		// No jobs available
		if(jobs.size() < 1){
		    activeIndex = -1;
		    return null;
		}
	
		// All right who's next? (index)
		if(activeIndex >= (jobs.size() - 1) || activeIndex < 0)
			activeIndex = 0;
		else {
		    ++activeIndex;
		}
		activeJob = jobs.get(activeIndex);
		quantumCounter = quantum;
		return activeJob;
    }

    public String getName() {
    	return "Round Robin";
    }
    
}