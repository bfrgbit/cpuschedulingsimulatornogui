#!/bin/bash

rm -rf tmp
rm -rf student.zip
mkdir -p tmp/CPUSchedulingSimulatorNoGUI
cp -r src lib cases_public refs_public samples tmp/CPUSchedulingSimulatorNoGUI
cp Makefile simu.sh tmp/CPUSchedulingSimulatorNoGUI
cp -r patch_student/* tmp/CPUSchedulingSimulatorNoGUI
cd tmp
zip -r student.zip CPUSchedulingSimulatorNoGUI
cd ..
cp tmp/student.zip .
rm -rf tmp
