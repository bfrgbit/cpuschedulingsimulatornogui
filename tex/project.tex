\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage[margin=1in]{geometry}
\usepackage{indentfirst}
\usepackage{flafter}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{lipsum}
\usepackage{changepage}
\usepackage{verbatim}
\usepackage{color}
\usepackage{xcolor}
\usepackage{mdframed}
 

\setlength{\parindent}{2em}
\setlength{\parskip}{0.5em}
\setlist{nolistsep}
\def\labelitemi{--}

\title{CS 143A - Principles of Operating Systems\\ 
	Programming Assignment: CPU Scheduling Simulator}
\author{Qiuxi (Charles) Zhu}


\begin{document}
\maketitle

\section{Introduction}

In this programming assignment, you will implement the CPU scheduling algorithms
we discussed in class.

You are given a code base for a CPU scheduling simulator, 
\texttt{CPUSchedulingSimulatorNoGUI}. This code base already has the core
components implemented to parse input files, simulate scheduling cycles, and
harvest statistics.
All you need to do is to implement the required algorithms.
Upon completion, 
when given a list of jobs and a scheduling algorithm, your simulator should
``run'' these jobs and output several metrics (turnaround time, etc.) of them.
By the end of the quarter, you will submit your source code. Your score will 
be based on the correctness of the simulator's output on a comprehensive set 
of test cases.

Please let me know if you encounter any problems 
that you believe to be a bug in this code.
I have tested most of it, but could have easily missed something subtle.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Overview}

You will go through several steps to complete this programming assignment and 
submit your work. Here is a checklist for your reference.

\newenvironment{checklist}{%
	\begin{itemize}{}{} % whatever you want the list to be
	\let\olditem\item
	\renewcommand\item{\marginpar{$\Box$} \olditem}
	\newcommand\checkeditem{\marginpar{$\CheckedBox$} \olditem }
}{%
	\end{list}
}

\begin{enumerate}
	\item Prepare a UNIX-like system environment,
		see Section \ref{environment}
	\item Setup necessary development utilities
	\item Get our code base for the CPU scheduling simulator,
		see Section \ref{codebase}
	\item Programming: First-come first-served,
		see Section \ref{alg_fcfs}
	\item Programming: Shortest job first,
		see Section \ref{alg_sjf}
	\item Programming: Single-queue priority,
		see Section \ref{alg_priority}
	\item Programming: Round robin,
		see Section \ref{alg_rr}
	\item Build your simulator and run simulation on one case,
		see Section \ref{building}
	\item Pack up your code for submission, see Section \ref{submission}
	\item Test your submission archive on public cases,
		see Section \ref{testing}
	\item Submit your submission archive to EEE dropbox
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Prerequisites}

The CPU scheduling simulator we provide you with is written in Java. Therefore, 
it is recommended that you use Java for this programming assignment. Also,
since our automated tools are developed in a UNIX-like system environment, you
will take the most advantage of these tools if you also work in a UNIX-like
environment. In this section,
we will discuss the required environment and
knowledge you need to complete this programming assignment.

\subsection{System Environment} \label{environment}

A UNIX-like system environment is recommended for automated compilation 
using \texttt{Makefile}. You can also benefit from such an environment by 
testing your simulator with the batch script that we will use to grade your
assignment. Supported UNIX-like environments are

\begin{itemize}
	\item Linux
	\item Mac OS X
	\item Microsoft Windows + Cygwin
	\item Microsoft Windows + MinGW
\end{itemize}

Since our code base is in Java, you will also need a Java Development Kit (JDK)
to compile and run the simulator.
You may use

\begin{itemize}
	\item OpenJDK 7/8 (\url{http://openjdk.java.net/install}), or 
	\item Oracle Java SE Development Kit 7/8
(\url{http://www.oracle.com/technetwork/java/javase/downloads})
\end{itemize}

If 
you use Linux, you should be able to install OpenJDK with your package manager.
You may choose any integrated development environment (IDE) you like,
or just use a plain-text editor for development. However, please remember
it is your 
responsibility to make sure that your code compiles correctly using
the \texttt{Makefile} we provide, and runs properly with the \texttt{simu.sh}
script.

The grading machine runs \textbf{Ubuntu 14.04.4 LTS (Trusty Tahr) 32-bit}
and \textbf{OpenJDK 7}.
Since the simulator is in Java, your code should be platform-independent.
We also 
suggest that you avoid using third-party Java packages
except for those we have already tested and included in the code base.
However, if you want to be sure that 
your code runs correctly on our machine, you can build
an Ubuntu 14.04.4 LTS (Trusty Tahr) virtual machine for yourself using
Oracle VirutalBox (\url{https://www.virtualbox.org/wiki/Downloads})
and the official Ubuntu 14.04.4 LTS (Trusty Tahr) ISO image
(\url{http://releases.ubuntu.com/trusty}).

\subsection{Knowledge and Skills}

The following knowledge and skills are required for this programming assignment:

\begin{itemize}
	\item Basic CPU scheduling algorithms
	\item Programming
	\item Object-oriented programming (OOP) (not much)
	\item Java (not much)
	\item UNIX-like system environment and use of terminal (not much)
\end{itemize}

You only need very basic knowledge and skill for OOP, Java, and UNIX terminal.
Therefore, we think it shouldn't be a big problem even if you don't have
any experience with them now.

However, if you believe that you cannot handle these and that 
you have to use a different system or programming language, we also
have other options for you, which
may include implementation of a complete simulator
in the language you choose, and/or demonstration to us face-to-face.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Getting the Code Base} \label{codebase}

The Java code base for the CPU scheduling simulator is available on our course
website. Please go ahead and check it out.
The following directory structure is included in this code base:

\begin{adjustwidth}{2em}{2em}
\begin{verbatim}
CPUSchedulingSimulatorNoGUI     // Code base root
|-- batch.sh                    // Batch script
|-- cases_public                // Public cases
|   \-- <...>
|-- lib                         // Dependencies
|   \-- <...>
|-- Makefile
|-- README.md
|-- refs_public                 // Reference outputs for public cases
|   \-- <...>
|-- samples                     // Sample cases
|   |-- sample_1.jobs.txt
|   |-- sample_2.jobs.txt
|   |-- sample_3.jobs.txt
|   |-- sample_4.jobs.txt
|   |-- sample_5.jobs.txt
|   \-- sample_6.jobs.txt
|-- simu.sh                     // Main simulation script
\-- src                         // Source
    |-- com
    |   \-- jimweller
    |       \-- cpuscheduler
    |           |-- BaseSchedulingAlgorithm.java
    |           |-- BetterFileFilter.java
    |           |-- CPUScheduler.java
    |           |-- FCFSSchedulingAlgorithm.java *
    |           |-- MainFileLoader.java
    |           |-- OptionallyPreemptiveSchedulingAlgorithm.java
    |           |-- PrioritySchedulingAlgorithm.java *
    |           |-- Process.java
    |           |-- RandomSchedulingAlgorithm.java
    |           |-- RoundRobinSchedulingAlgorithm.java *
    |           |-- SchedulingAlgorithm.java
    |           \-- SJFSchedulingAlgorithm.java *
    \-- manifest.mf
\end{verbatim}
\end{adjustwidth}

The four \texttt{.java} files marked with asterisks (\texttt{*}) are
scheduling algorithms for you to implement.
\texttt{RandomSchedulingAlgorithm.java} contains an example of implemented
scheduling algorithm that will not be used for grading.
This algorithm chooses a random job in the queue to execute for
each cycle. You may refer to it when you write your own algorithms.

We suggest you not touch other files in the code base unless you know exactly 
what you are doing. If you find an issue and you believe it is a bug in other
files, please try your best to find out where it is likely to be 
and post a bug report on Piazza.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Programming}

This section describes the scheduling algorithms that you will implement.
I have included the skeleton code for each algorithm in the corresponding
\texttt{.java} file, so you only need to fill in the blanks in 
a few specified functions, 
adding additional members and auxiliary functions as necessary.

The most important source code file is
\texttt{SchedulingAlgorithm.java}, which is
reproduced next for your convenience, is the 
interface that defines all of the methods that your scheduling algorithm classes
must implement.
This allows you to write any number of scheduling algorithm classes
implementing this interface.

% \pagebreak
\lstset{
	numbers=left,
	numberstyle=\ttfamily,
	numbersep=1em,
	captionpos=b
} 
\lstset{basicstyle=\ttfamily}
\lstset{framesep=1em}
\lstset{breaklines=true}
\definecolor{mygreen}{rgb}{0, 0.4, 0.2}
\definecolor{myblue}{rgb}{0, 0, 0.6}
\definecolor{mymauve}{rgb}{0.3, 0, 0.3}
\lstset{
	commentstyle=\color{mygreen},
	keywordstyle=\color{myblue},
	stringstyle=\color{mymauve}
}
\lstinputlisting[language=Java, firstline=11]
	{../src/com/jimweller/cpuscheduler/SchedulingAlgorithm.java}

The second most important source code file is
\texttt{BaseSchedulingAlgorithm.java}.
Extend this class in each of yours
(already done in the provided skeleton code classes)
in order to inherit some of the
methods defined in this class and avoid rewriting them.
In particular, note the
\texttt{isJobFinished()} function and the \texttt{activeJob} member,
which are useful for keeping
track of the currently running job and determining whether to preempt it or not.

The third most important source code file is \texttt{Process.java}.
Feel free to use any of the public functions in the latter half of it
(from \texttt{getResponseTime()} onwards).
In particular, note the \texttt{getBurstTime()} function,
which returns the remaining CPU burst time of the specified \texttt{Process}.
Do not access the members directly from your code.

\textbf{Only one of the algorithms will be active in each simulation,} from 
the beginning to the end.
Therefore, you could just leave
\texttt{transferJobsTo()} unimplemented.

\subsection{First-Come First-Served (10 points)} \label{alg_fcfs}

A simple FCFS algorithm. If there is a tie, use PID as the tie-breaker
(smaller-PID first).

\subsection{Shortest Job First (25 points)} \label{alg_sjf}

This algorithm should run the job with the \emph{shortest remaining time} first.
If there is a tie, use PID as the tie-breaker
(smaller-PID first).

It should implement the \texttt{OptionallyPreemptiveSchedulingAlgorithm}
interface and preempt the currently running processes
only if it is set to be preemptive. The non-preemptive operation is worth 15
points, and the preemptive operation is worth 10 points.

\subsection{Single-Queue Priority (25 points)} \label{alg_priority}

This algorithm should run the job with the \emph{highest priority}
(lowest priority number). If there is a tie, use PID as the tie-breaker
(smaller-PID first). 

It should implement the \texttt{OptionallyPreemptiveSchedulingAlgorithm}
interface and preempt currently running processes
only if it is set to be preemptive. The non-preemptive operation is worth 15
points, and the preemptive operation is worth 10 points.

\begin{mdframed}[backgroundcolor=lightgray]
\noindent \textbf{Hint:} The SJF and single-queue priority algorithms can be implemented 
in almost the same way. How about writing a helper method and
let one algorithm extend the other?
\end{mdframed}

\subsection{Round Robin (30 points)} \label{alg_rr}

We recommend working on this algorithm last,
as it is by far the most complicated.
You must maintain state between each call to
\texttt{getNextJob(int currentTime)},
which can be difficult if you are not careful.
The default time quantum is 10. The actual time quantum we use for grading might
be different.
Note that the skeleton code includes some methods for
setting/getting the current quantum.
Don't modify them as they are required by the simulator.

\begin{mdframed}[backgroundcolor=lightgray]
\noindent \textbf{Hint:} Round Robin scheduling doesn't put requirements on the 
underlying implementation. Therefore, 
with different implementation the outcome can be different.

\noindent To make sure your Round Robin outputs are consistent with the 
reference outputs, please maintain a list of unfinished jobs in increasing
order of their PIDs. 
In the case of job completion or preemption,
switch to the next job (if does not exist, the first job) in the list.
\end{mdframed}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Building the Simulator} \label{building}

This section describes how to compile/build your simulator, and run a simulation
with a list of jobs and a specified algorithm.

\subsection{Compiling} \label{compiling}

As is described in Section \ref{environment}, you will need a UNIX-like system
environment that allows you to use the ``Makefile'', and a 
Java Development Kit (JDK) to compile your code and run your program.

Follow these steps to compile the simulator:

\begin{enumerate}
	\item Open your terminal.
	\item Navigate to the \texttt{CPUSchedulingSimulatorNoGUI} folder.
	\item Run \texttt{make} (or \texttt{make all}) to compile the source code.
\end{enumerate}

If successful, 
this will create several compiled classes under \texttt{bin}
and generate a \texttt{.jar} file
named \texttt{CPUSchedulingSimulator.jar}, which contains all the classes 
and dependencies. Again, if you use an integrated development environment (IDE),
please make sure your simulator also compiles correctly with the
\texttt{Makefile} we provide, before your make a submission.

\subsection{Running a Single-Case Simulation}

To run the simulator, change the working directory to 
\texttt{CPUSchedulingSimulatorNoGUI} and run \texttt{./simu.sh} 
or \texttt{bash simu.sh}. This script checks if you have a valid JVM in your
system \texttt{PATH}, and invokes \texttt{java -jar CPUSchedulingSimulator} 
(passing all parameters)
for you.
If your system environment is configured properly and the JAR file is made
successfully during compilation in Section \ref{compiling}, 
running such a command without any parameter
will print out the following usage information:

\begin{adjustwidth}{2em}{2em}
\begin{verbatim}
Usage: java -jar CPUSchedulingSimulator.jar JOBS ALGORITHM
       java -jar CPUSchedulingSimulator.jar JOBS ALGORITHM [PREEMPT]
       java -jar CPUSchedulingSimulator.jar JOBS RR [QUANTUM]
Run the list of jobs using the specified algortihm.

Supported algorithms are:
    RR                  Round Robin
    Random              Random Job
    Priority            Single-Queue Priority (Optionally Preemptive)
    FCFS                First-Come First-Served
    SJF                 Shortest Job First (Optionally Preemptive)

QUANTUM is an option if ALGORITHM is RR (Round Robin).
The default QUANTUM is 10.
\end{verbatim}
\end{adjustwidth}

To actually run the simulator with a list of jobs and an algorithm, you will 
need to provide at least two parameters:
\texttt{JOBS} -- The path to a configuration file that contains a list of jobs,
and \texttt{ALGORITHM} -- The identifier of a supported algorithm as is listed 
above.

The optional \texttt{PREEMPT} parameter is applicable to algorithms listed as
``optionally preemptive''. It can be set to \texttt{true} or \texttt{false}.
When set to \texttt{true}, the algorithm should run in the preemptive way.
When set to \texttt{false} or not provided,
the algorithm should run in the non-preemptive way.

The optional \texttt{QUANTUM} parameter is applicable to the Round Robin
(\texttt{RR}) scheduling algorithm only. It can be set to a positive integer,
e.g. 20. If not provided, the default value 10 will be used.

Here are a few examples of single-case simulation commands:

\begin{adjustwidth}{2em}{2em}
\begin{verbatim}
./simu.sh samples/sample_1.jobs.txt Random
./simu.sh samples/sample_2.jobs.txt FCFS
./simu.sh samples/sample_4.jobs.txt SJF
./simu.sh samples/sample_4.jobs.txt SJF true
./simu.sh samples/sample_3.jobs.txt Priority
./simu.sh samples/sample_3.jobs.txt Priority true
./simu.sh samples/sample_5.jobs.txt RR
\end{verbatim}
\end{adjustwidth}

The simulator prints out the metrics in a CSV structure like this:

\begin{adjustwidth}{2em}{2em}
\begin{verbatim}
Single-Queue Priority,,Non-preemptive
"PID","Burst","Priority","Arrival","Start","Finish","Wait","Response",
"Turnaround"
1001,300,2,0,0,299,0,0,300
1002,200,1,10,400,599,390,390,590
1003,100,0,20,300,399,280,280,380
,,,,,,,,
,,,,,Min,0,0,300
,,,,,Mean,223.33,223.33,423.33
,,,,,Max,390,390,590
,,,,,StdDev,201.08,201.08,149.78
\end{verbatim}
\end{adjustwidth}

You can use this single-case simulation 
to test your program with sample cases provided in the \texttt{samples}
folder to make sure nothing crashes during runtime.
The public cases 1--5 are the same with the sample cases.
Therefore, you can also
compare your output with the provided reference outputs
in \texttt{refs\_public}.

\subsection{Creating Job Lists}
\label{process_data}

Job lists are stored in the \texttt{*.jobs.txt} files. You can find a bunch of such 
files in \texttt{samples} and \texttt{cases\_public}.
These files have the following format for each line:

\texttt{BURST DELAY PRIORITY}

\noindent where each of these is an integer text value and is
separated by a space,
with no leading or trailing spaces. \texttt{BURST} should be positive, while
\texttt{DELAY} and \texttt{PRIORITY} should be non-negative. You can assume 
these conditions always hold for test cases we use for grading.
For example, the content of
\texttt{samples/sample\_4.jobs.txt} is as follows:

\begin{adjustwidth}{2em}{2em}
\verbatiminput{../samples/sample_4.jobs.txt}
\end{adjustwidth}

Note that the \texttt{DELAY} of these jobs are respectively 0, 10, 10, which
means the arrival time of these jobs are actually 0, 10, 20.

For randomly generated job lists, the \texttt{BURST} of each job is between
1--500, \texttt{DELAY} is between 0--199, and \texttt{PRIORITY} is between 0--9.
You are not required to handle invalid input files (overflowing integers, etc.)
At the same time,
you are welcomed to make your own job lists and share them with the class.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Making a Submission} \label{submission}

When you are done with implementation of all the required algorithms and 
confirmed that your simulator compiles properly with the
\texttt{Makefile} we provide in the code base, you are ready to make 
a submission. You may have noticed that the scores of four scheduling 
algorithms add up to 90 points. The last 10 points are given to your 
appropriate submission. 

Follow these steps to pack up your source code for submission:

\begin{enumerate}
	\item Open your terminal.
	\item Navigate to the \texttt{CPUSchedulingSimulatorNoGUI} folder.
	\item Run \texttt{make submission} to compress your source code into a
		\texttt{.zip} file.
\end{enumerate}

Please note that you are \textbf{REQUIRED}
to prepare your assignment submission 
with the steps above. Failing to do so will add to our work load and lower
your final score.

If successful, 
this will create a \texttt{.zip} file
named \texttt{submission.zip}, which contains all the \texttt{.java} 
source code files under \texttt{src},
the \texttt{Makefile}, the \texttt{simu.sh} script, and all dependencies
under \texttt{lib}. Please note that
these operations \textbf{WILL NOT} actually submit your assignment to EEE for you.
It does nothing but packing up your source code, as well as some dependencies,
into a \texttt{.zip} file. You will need to submit the \texttt{.zip} file
manually.

Before actual submission to EEE, 
you may want to test your simulator with the public test cases we prepared for
you. The \texttt{submission.zip} file is also the one you will use for testing.
Please refer to Section \ref{testing} for details.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Testing on the Public Cases} \label{testing}

In this section, we talk about the automated grading tools. As is mentioned in 
Section \ref{environment}, a UNIX-like system environment is required for these
tools. The tools can also be found on our course website.
Please go ahead and check it out.

In addition to what you need for compilation, you will also need Ruby to 
run the grading script.
If you are using Linux, you should be able to install it with your package 
manager.
If you are using the latest Mac OS X,
you should already have it, so check it out before trying any \texttt{brew}
based installation guide.
If you are using Windows, you will need to use RubyInstaller to set it
up for you. Make sure you add it to your system \texttt{PATH}
during installation.
Instruction on installing Ruby can be found on its official website
at \url{https://www.ruby-lang.org/en/documentation/installation}.

Upon successful configuration of your environment, you should be able to test
your simulator with our grading tools. Extract the grading tools, and you will
get a \texttt{CPUSchedulingGrader} folder.
The grading tools come with public test cases and the reference outputs, so you
don't need to prepare them yourself.

Follow these steps to test your submission:

\begin{enumerate}
	\item Copy your \texttt{submission.zip} to the \texttt{CPUSchedulingGrader}
		folder.
	\item Rename your \texttt{submission.zip} with your student ID number as 
		appears in your EEE account,
		which should be eight digits or ``X'' plus seven digits. For example,
		if your student ID number is 12345678, then the file should be renamed
		to \texttt{12345678.zip}.
	\item Open your terminal.
	\item Navigate to the \texttt{CPUSchedulingGrader} folder.
	\item Run \texttt{./run.sh 12345678.zip} or 
		\texttt{bash run.sh 12345678.zip} to automatically extract, compile, and
		run your code.
	\item Run \texttt{./grade.sh s\_12345678} to compare your outputs with 
		reference outputs and get your scores for each algorithm and your
		final score on the public cases.
\end{enumerate}

Note that we are going to use a private test set which contains similar but 
difference cases. Therefore, your actual score might be difference with your
testing score.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Possible Issues}

\subsection{Setting Up the System \texttt{PATH}}

The most common issue, especially for Microsoft Windows users,
is related to setting up the system \texttt{PATH}. Basically it is to 
find your ``Environment Variables'' in your ``System Properties'' or 
``Advanced System Settings'', and add the folders that contain the 
executables of your tools to the \texttt{PATH} variable.

If you get errors like ``not recognized as a command'' or ``command not found''
on some software you already installed, 
it is likely that your system \texttt{PATH} is not configured correctly to 
include the path to its executable(s).

This page talks about setting up \texttt{PATH} for
Oracle Java SE Development Kit: 
\url{https://java.com/en/download/help/path.xml}.
This question on Stack Overflow has a bunch of answers with screen shots showing 
this in different Windows editions:
\url{http://stackoverflow.com/questions/1672281/
environment-variables-for-java-installation}.

\subsection{Fixing File Permissions}

If you get a ``permission denied'' error running any of our scripts, please
check if you have execute permissions for the \texttt{.sh} BASH scripts.
In a UNIX-like environment, to make these scripts executable, you need to 
navigate to the folder that contains those scripts, and run
\texttt{chmod +x *.sh}.

\subsection{Setting Up a UNIX-like Environment in Microsoft Windows}

You will need Cygwin (\url{https://www.cygwin.com}) or
MinGW (\url{http://www.mingw.org}) to create a UNIX-like environment in 
Microsoft Windows to run BASH scripts and use the \texttt{Makefile}. Check their
official websites for installation instructions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Using Other Systems/Languages}

We highly recommend that you use a UNIX-like system environment and Java for 
this programming assignment, because we already have the simulator core
implemented in the code base and the automated tools created for you, which
should save you a lot of time.

However, if you really believe that you cannot handle these tools, you will need
to talk to our TAs or instructors in person to make a plan for you. This may
involve implementation of a complete simulator
in the language you use that acts in EXACTLY THE SAME way 
as the simulator we have,
and/or demonstration to us face-to-face.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Acknowledgment}

Thanks to Jim Weller for providing the framework for the scheduling simulator.
Thanks to Kyle Benson for improving the simulator and writing the algorithms.

\end{document}          
